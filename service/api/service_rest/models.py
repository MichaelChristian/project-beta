from django.db import models

class Manufacturer(models.Model):
    default = models.CharField(max_length=100)
    carManufacturer = models.CharField(max_length=100),
    carManufacturerModel = models.CharField(max_length=100),
    carManufacturerAmmenities = models.CharField(max_length=100),
    carManufacturerECU = models.CharField(max_length=100)
    carManufacturerTypeRotor = models.CharField(max_length=100)

    def __str__(self):
        return self.default

    class Meta:
        ordering = ("default",)

class serviceAppointment(models.Model):
    appointment = models.CharField(max_length=100),
    date = models.DateTimeField(unique=True), 
    routineMaintenance = (models.TextField),
    consumer = models.CharField(max_length=100)
    
class AutomobileVO(models.Model):
    color = models.CharField(max_length=100),
    year = models.CharField(max_length=100),
    vin = models.CharField(unique = True, max_length = 20),

class Technician(models.Model):
    name = models.CharField(max_length = 100),
    employee_number = models.CharField(max_length = 100),
    







