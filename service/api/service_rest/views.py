import json
from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder
from inventory.api.inventory_rest.encoders import ManufacturerEncoder
from inventory.api.inventory_rest.models import Manufacturer
from django.views.decorators.http import require_http_methods
from service_rest.models import Manufacturer, serviceAppointment




# Create your views here.
class ManufacturerEncoder:
    model = Manufacturer
    properties = ['carManufacturer']

class serviceAppointmentEncoder(ModelEncoder): 
    model = serviceAppointment
    def __init__(self, appointment, date, routineMaintenance, consumer): 
        properties = [
            appointment,
            date,
            routineMaintenance,
            consumer,
        ]
        return self(properties.date)

@require_http_methods(["GET", "POST"])
def upcomingAppointments(request):
    if request.method == "GET":
        serviceAppointment = serviceAppointment.objects.all()
        return JsonResponse(
            {"serviceAppointment": serviceAppointment},
            encoder=serviceAppointmentEncoder,
        )
    else:
        content = json.loads(request.body)

        #location of the service appointment
        try:
            location = serviceAppointment.objects.get(id=content["location"])
            content["location"] = location
        except serviceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

#showing conference can be a number of different things with decorators
#such as "GET" and "POST" here
@require_http_methods(["GET", "POST"])
def showAppointments(request, pk):
    if request.method == "GET":
        scheduleMaitenance = serviceAppointment.objects.get(id=pk)
        return JsonResponse (
            scheduleMaitenance,
            encoder=ManufacturerEncoder, safe=False )  
    else:
        request.method == "DELETE"
        try:
            if serviceAppointment.empty() == True:
                serviceAppointment.objects.get(id=pk)
                serviceAppointment.delete()
                print( JsonResponse(
                    serviceAppointment,
                    encoder=serviceAppointmentEncoder,
                    safe=False,
            ))
        except:
                if serviceAppointment.DoesNotExist:
                    response = JsonResponse({"message": "Does not exist"})
                    response.status_code = 404
                    print (response ({"message": "Does not exist"}))
                else:
                    print("console")