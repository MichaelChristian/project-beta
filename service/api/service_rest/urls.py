from django.urls import path
from .views import path


from service_rest.models import (
default,
carManufacturer,
carManufacturerModel,
carManufacturerAmmenities,
carManufacturerECU,
carManufacturerTypeRotor
)

urlpatterns = [
    path(
        "service/",
        default,
        name="api_automobiles",
    ),
    path(
        "automobiles/<str:vin>/",
        carManufacturer,
        name="api_automobile",
    ),
    path(
        "manufacturers/",
        carManufacturerModel,
        name="api_manufacturers",
    ),
    path(
        "manufacturers/<int:pk>/",
        carManufacturerECU,
        name="api_manufacturer",
    ),
    path(
        "models/",
        carManufacturerAmmenities,
        name="api_vehicle_models",
    ),
    path(
        "models/<int:pk>/",
        carManufacturerTypeRotor,
        name="api_vehicle_model",
    ),
]