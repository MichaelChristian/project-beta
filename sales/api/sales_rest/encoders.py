from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold'
        ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        'id',
        'name'
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'address',
        'phone_number'
    ]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'id',
        'name',
        'employee_id'
    ]

class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'name',
        'employee_id'
    ]

class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'id',
        'sales_person',
        'price'
    ]
    def get_extra_data(self, o):
        return {
            "automobile": [o.automobile.vin, o.automobile.sold],
            "customer": o.customer.name,
            "sales_person": [o.sales_person.name, o.sales_person.employee_id]
        }
#     encoders = {
#         'salesperson': SalesPersonDetailEncoder(),
#         'customer': CustomerDetailEncoder(),
#         'automobile': AutomobileVODetailEncoder()
# }


class SalesRecordDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'automobile',
        'sales_person',
        'customer',
        'price'
    ]
    def get_extra_data(self, o):
        return {
        "automobile": [o.automobile.vin, o.automobile.sold],
        "customer": o.customer.name,
        "sales_person": [o.sales_person.name, o.sales_person.employee_id]
        }
    # encoders = {
    #     'salesperson': SalesPersonDetailEncoder(),
    #     'customer': CustomerDetailEncoder(),
    #     'automobile': AutomobileVODetailEncoder
    # }
