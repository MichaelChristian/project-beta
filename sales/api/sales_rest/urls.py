from django.urls import path
from .views import api_list_sales_person, api_show_sales_person, api_list_customer, api_show_customer, api_list_sales_record, api_show_sales_record

urlpatterns = [
    path('salesperson/', api_list_sales_person, name='api_list_sales_person'),
    path('salesperson/<int:pk>/', api_show_sales_person, name='api_show_sales_person'),
    path('customers/', api_list_customer, name='api_list_customer'),
    path('customers/<int:pk>/', api_show_customer, name='api_show_customer'),
    path('salesrecords/', api_list_sales_record, name='api_list_sales_record'),
    path('salesrecords/<int:pk>/', api_show_sales_record, name='api_show_sales_record')
]
