from django.contrib import admin
from .models import AutomobileVO, SalesRecord
# Register your models here.
admin.site.register(AutomobileVO)
admin.site.register(SalesRecord)
