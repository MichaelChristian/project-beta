from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=50)

class SalesRecord(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name="automobiles", on_delete=models.PROTECT)
    sales_person = models.ForeignKey(SalesPerson, related_name="sales_persons", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="customers", on_delete=models.PROTECT)
    price = models.CharField(max_length=50)
