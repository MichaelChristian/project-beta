from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
SalesPersonListEncoder,
SalesPersonDetailEncoder,
SalesRecordDetailEncoder,
SalesRecordListEncoder,
CustomerDetailEncoder,
CustomerListEncoder)
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord

# Create your views here.
@require_http_methods(['GET', 'POST'])
def api_list_sales_person(request):
    if request.method == 'GET':
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {'sales_person': sales_person},
            encoder=SalesPersonListEncoder
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonDetailEncoder,
            safe=False
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_sales_person(request, pk):
    if request.method == 'GET':
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonDetailEncoder,
            safe=False
        )
    elif request.method == 'PUT':
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonDetailEncoder,
            safe=False
        )
    else:
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})

@require_http_methods(['GET', 'POST'])
def api_list_customer(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            {'customers': customers},
            encoder=CustomerListEncoder
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_customer(request, pk):
    if request.method == 'GET':
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    elif request.method == 'PUT':
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})

@require_http_methods(['GET', 'POST'])
def api_list_sales_record(request):
    if request.method == 'GET':
        sales_record = SalesRecord.objects.all()
        return JsonResponse(
            {'sales_record': sales_record},
            encoder=SalesRecordListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            content = json.loads(request.body)

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordListEncoder,
                safe=False
            )

        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid salesperson"}
            )
            response.status_code = 400
            return response

        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid customer"}
            )
            response.status_code = 400
            return response

        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid automobile"}
            )
            response.status_code = 400
            return response



@require_http_methods(['GET','PUT','DELETE'])
def api_show_sales_record(request,pk):
    if request.method == 'GET':
        sales_record = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordDetailEncoder,
            safe=False
        )
    elif request.method == 'PUT':
        content = json.loads(request.body)
        SalesRecord.objects.filter(id=pk).update(**content)
        sales_record = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordDetailEncoder,
            safe=False
        )
