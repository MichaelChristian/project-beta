import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
            <NavLink className="navbar-brand" to="/customers/new/">Add a new customer</NavLink>
          </li>
          <li className='nav-item'>
            <NavLink className="navbar-brand" to="/salesperson/new/">Add a sales person</NavLink>
          </li>
          <li className='nav-item'>
            <NavLink className="navbar-brand" to="/salesrecords/list/">List of sales</NavLink>
          </li>
          {/* <li className='nav-item'>
            <NavLink className="navbar-brand" to="/salesrecords/new/">Create a sales record</NavLink>
          </li> */}
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
