import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddCustomerForm from './Sales/AddCustomerForm';
import AddSalesPersonForm from './Sales/AddSalesPersonForm';
import SaleList from './Sales/ListSales';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers">
            <Route path="new" element={<AddCustomerForm />} />
          </Route>
          <Route path="salesperson">
            <Route path="new" element={<AddSalesPersonForm />} />
          </Route>
          <Route path="salesrecords">
            <Route path="list" element={<SaleList />} />
            {/* <Route path="new" element={<CreateSalesRecord />} /> */}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
