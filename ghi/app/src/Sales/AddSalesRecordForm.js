import React from 'react'

class AddSalesRecordForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            automobile: '',
            sales_person: '',
            customer: '',
            price: '',
            automobiles: [],
            sales_persons: [],
            customers: []
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this)
    this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this)
    this.handleChangePrice = this.handleChangePrice.bind(this)
    this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        delete data.automobiles
        delete data.sales_persons
        delete data.customers

        const salesRecordUrl = 'http://localhost:8090/api/salesrecords/'
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const automobileUrl = 'http://localhost:8100/api/automobiles/${data.automobile}/'
        const automobileFetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const recordResponse = await fetch(salesRecordUrl, fetchOptions)
        const automobileResponse = await fetch(automobileUrl, automobileFetchOptions)

        if (recordResponse.ok && automobileResponse.ok) {
            const cleared = {
                automobile: '',
                sales_person: '',
                customer: '',
                price: ''
            }
            this.setState(cleared)
        }
    }
    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value })
    };

    handleChangeSalesPerson(event) {
        const value= event.target.value;
        this.setState({ sales_person: value })
    }

    async componentDidMount() {
        const salesUrl = 'http://localhost:8090/api/salesperson/'
        const salesResponse = await fetch(salesUrl)
        if (salesResponse.ok) {
            const salesPersonData = await salesResponse.json()
            this.setState ({ sales_persons: salesPersonData.sales_person })
        }

        const customerUrl = 'http://localhost:8090/api/customers/'
        const customerResponse = await fetch(customerUrl)
        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            this.setState ({ customers: customerData.customers })
        }

        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const automobileResponse = await fetch(automobileUrl)
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json()
            this.setState ({ automobiles: automobileData.automobiles })
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create Sales Record</h1>
                <form onSubmit={this.handleSubmit} id="add-sales-record">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeCustomer} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="Name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="Address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangePhone} value={this.state.phone_number} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="Phone Number">Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

// export default AddSalesRecordForm
