import React from 'react'

class AddSalesPersonForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: '',
            employee_id: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmpid = this.handleChangeEmpid.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        const salesPersonUrl = 'http://localhost:8090/api/salesperson/'
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const submitResponse = await fetch(salesPersonUrl, fetchOptions);
        if (submitResponse.ok) {
            this.setState({
                name: '',
                employee_id: '',
            })
        }
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleChangeEmpid(event) {
        const value = event.target.value;
        this.setState({ employee_id: value })
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Sales Person</h1>
                <form onSubmit={this.handleSubmit} id="add-salesperson-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="Sales Person">Sales Person</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeEmpid} value={this.state.employee_id} placeholder="employee-id" required type="text" name="employee-id" id="employee-id" className="form-control" />
                    <label htmlFor="Employee_id">Employee ID</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
export default AddSalesPersonForm
