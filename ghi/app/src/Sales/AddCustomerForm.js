import React from 'react'

class AddCustomerForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: '',
            address: '',
            phone_number: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this)
        this.handleChangeAddress = this.handleChangeAddress.bind(this)
        this.handleChangePhone = this.handleChangePhone.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const url = 'http://localhost:8090/api/customers/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const customerResponse = await fetch(url, fetchOptions);
        if (customerResponse.ok) {
            this.setState({
                name: '',
                address: '',
                phone_number: '',
            })
        }
    }
    handleChangeCustomer(event) {
        const value = event.target.value
        this.setState({ name: value})
    }
    handleChangeAddress(event) {
        const value = event.target.value
        this.setState({ address: value})
    }
    handleChangePhone(event) {
        const value = event.target.value
        this.setState({ phone_number: value})
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Customer</h1>
                <form onSubmit={this.handleSubmit} id="add-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeCustomer} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="Name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="Address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangePhone} value={this.state.phone_number} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="Phone Number">Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
export default AddCustomerForm
