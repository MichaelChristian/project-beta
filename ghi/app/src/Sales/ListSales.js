import React from "react";

class SaleList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales: []
        }
    }
    async loadSales() {
        const salesUrl = "http://localhost:8090/api/salesrecords/"
        const response = await fetch(salesUrl)

        if (response.ok) {
            const data = await response.json()
            this.setState({ sales: data.sales_record })
        }
    }

    componentDidMount() {
        this.loadSales()
    }

    render() {
        console.log(this.state)
        return(
            <table className="table table-striped">
            <thead>
              <tr>
                <th>Automobile VIN</th>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.sales.map(sale => {
                return (
                  <tr key={ sale.id }>
                    <td>{ sale.automobile[0] }</td>
                    <td>{ sale.sales_person[1] }</td>
                    <td>{ sale.customer }</td>
                    <td>{ sale.price }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )
            }
}

export default SaleList
