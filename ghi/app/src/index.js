import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const response = fetch ('http://localhost:8090/api/salesperson/')
console.log(response)
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
