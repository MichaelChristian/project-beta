# CarCar

Team:

* Person 1 - Michael Christian - Sales
* Person 2 - Which microservice? Kyle = Service

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Starting off, it was important that the sales microservice was given a good structure from our models. This was done by creating a sales person, a customer, and a sales record. It was also important that a value object model was created in order to use for our poller. This integrates with the inventory microservice and will poll data giving us the vin from the automobiles within that microservice. Sales persons consist of a name, and a unique identifier which is an employee id. The customer model consists of a name, an address, and a phone number. Finally, the sales records have a foreign key relationship with the three other models. This gives the sales records a customer, a sales person, and the automobile, which is from our value object. A price is also included within the sales record model. Sales records will still exist if any of these relationships are deleted from the database.
